const { createApp, reactive, computed, ref, watch } = Vue
const { createVuetify } = Vuetify

const vuetify = createVuetify({
	theme: {
		defaultTheme: 'light',
	},
})

createApp({
	setup() {
		const deepClone = ((obj) => JSON.parse(JSON.stringify(obj)))
		const calcFrom = [
			{ title: 'Beginning', value: 0 },
			{ title: 'Aligned Arrows', value: 1 },
		]
		const selected_calc_from = ref(0)
		const show_how_to_choose_dialog = ref(false)
		const show_more = ref(false)
		const show_helper_dialog = ref(false)
		const requested_offset = ref('')
		let listsOfSmithingActions = ref([])
		const smithing_actions = {
			// actions with negative values
			hit_light: { name: "Light Hit", value: -3, max_calc_loops: 3, img_offset: -128 },
			hit_medium: { name: "Medium Hit", value: -6, max_calc_loops: 3, img_offset: -160 },
			hit_hard: { name: "Hard Hit", value: -9, max_calc_loops: 3, img_offset: -192 },
			draw: { name: "Draw", value: -15, max_calc_loops: 9, img_offset: -224 },
			// actions with positive values
			punch: { name: "Punch", value: 2, max_calc_loops: 3, img_offset: 0 },
			bend: { name: "Bend", value: 7, max_calc_loops: 3, img_offset: -32 },
			upset: { name: "Upset", value: 13, max_calc_loops: 3, img_offset: -64 },
			shrink: { name: "Shrink", value: 16, max_calc_loops: 9, img_offset: -96 },
		}

		// map out a more html v-for friendly version of the above
		// and add None as the first option
		const smithingActions = [{
			name: "None", value: 0, img_offset: -256, key: "none"
		}].concat(Object.keys(smithing_actions).map((key) => {
			let entry = deepClone(smithing_actions[key])
			entry.key = key
			return entry
		}))

		const smithingActionsForSelect = smithingActions.slice(0,1).concat({
			name: "Hit", value: -3, img_offset: -128, key: "hit"
		}).concat(smithingActions.slice(4))

		// create one that is better for calculating with
		const smithingActionsForCalc = smithingActions.slice(1,5).concat(smithingActions.slice(5).reverse())

		// create one that just has the "hit" actions for use in soe calculations
		const smithingHits = smithingActions.reduce((result, action) => {
			if(action.key.substring(0,3) === 'hit') {
				result.push(action)
			}
			return result
		}, [])

		const showHowToChooseCalcFrom = (() => {
			show_how_to_choose_dialog.value = true
		})
		const closeHowToChooseCalcFrom = (() => {
			show_how_to_choose_dialog.value = false
		})

		const showUserDefinedSmithingValue = computed(() => {
			return selected_calc_from.value === 0
		})
		watch(selected_calc_from, async (new_val, old_val) => {
			if(new_val === 1) {
				requested_offset.value = ''
			}
		})
		
		const smithingHelperValues = Object.keys(smithing_actions).map((key) => {
			let entry = deepClone(smithing_actions[key])
			entry.key = key
			entry.count = ref('')
			return entry
		})
		const smithingHelperTotal = computed(() => {
			const getCount = ((item) => {
				let count = parseInt(item.count.value)
				return isNaN(count) ? 0 : count
			})
			return smithingHelperValues.reduce((result, item) => { return result + (getCount(item) * item.value) }, 0)
		})
		const showHelperDialog = (() => {
			show_helper_dialog.value = true
		})
		const closeHelperDialog = (() => {
			show_helper_dialog.value = false
			smithingHelperValues.forEach((item, index) => {
				smithingHelperValues[index].count.value = '' // reset all helper values
			})
		})
		const useHelperValue = (() => {
			requested_offset.value = smithingHelperTotal.value
			listsOfSmithingActions.value = []
			closeHelperDialog()
		})

		const lastActions = reactive([
			smithingActions[0],
			smithingActions[0],
			smithingActions[0],
		])
		
		const noSuggestions = computed(() => {
			return listsOfSmithingActions.value.length === 0
		})
		
		const isCalculating = computed(() => {
			return listsOfSmithingActions.value.length === 1 && listsOfSmithingActions.value[0] === 'Calculating...'
		})
		
		const hasSuggestions = computed(() => {
			return !noSuggestions.value && !isCalculating.value
		})
		
		const smithingOffset = ((dynamic_last_actions) => {
			const last_actions_offset = dynamic_last_actions.reduce((result, action) => {
				return result + action.value
			}, 0)
			let ro = parseInt(requested_offset.value)
			return (isNaN(ro) ? 0 : ro) - last_actions_offset
		})

		const lastActionsList = computed(() => {
			return lastActions.map((action) => { return action.key.substring(0,3) === 'hit' ? smithingHits : action })
		})

		const formatLastActions = ((last_actions_to_format) => {
			return last_actions_to_format.reduce((result, action) => {
				if(action.value !== 0) {
					// check for the action in the result
					let index = result.findIndex((a) => a.value === action.value)
					// if one of the following is true:
					// - the result is empty
					// - the action is not in the result
					// - the action is in the result but is not the last action
					// (this last one is because we only want to group actions that are next to each other)
					// then do this
					if(result.length == 0 || index == -1 || index+1 !== result.length) {
						action.count = 1
						result.push(action)
					} else { // else do this
						result[index].count += 1
					}
				}
				return result
			}, []).reverse().map((action) => { return action.count+'x '+action.name })
		})
		
		let actions_tracker = []
		const optimizeActionsGrouping = ((formatted_actions_list) => {
			let getStepParts = ((formatted_step) => {
				let regex = new RegExp('(?<steps>(^[0-9]+))x (?<name>([a-zA-Z ]+$))')
				let match = formatted_step.match(regex)
				return match !== null ? match.groups : match
			})
			let actions_list = formatted_actions_list.map((formatted_action) => getStepParts(formatted_action))
			let optimized_actions_list = [actions_list[0]]
			let a = 0
			for(let i = 1; i < actions_list.length; i++) {
				if(optimized_actions_list[a].name === actions_list[i].name) {
					optimized_actions_list[a].steps = parseInt(optimized_actions_list[i-1].steps) + parseInt(actions_list[i].steps)
				} else {
					optimized_actions_list.push(actions_list[i])
					a += 1
				}
			}
			return optimized_actions_list.map((action) => action.steps + 'x ' + action.name)
		})
		const calcSmithingActionsRecursive = ((actions, dynamic_last_actions, offset = 0, stepcount_ary = [], lowest_stepcount = 99) => {
			let action = actions[0]
			let base_stepcount = stepcount_ary.reduce((result, s) => { return result + s.steps }, 0)
			for(let i = 0; i < action.max_calc_loops; i++) {
				let cur_stepcount = base_stepcount + i
				let new_offset = offset + action.value * i
				if(actions.length > 1) {
					lowest_stepcount = calcSmithingActionsRecursive(actions.slice(1), dynamic_last_actions, new_offset, stepcount_ary.concat({ name: action.name, steps: i }), lowest_stepcount)
				} else if(new_offset === smithingOffset(dynamic_last_actions) && cur_stepcount <= lowest_stepcount) {
					if(cur_stepcount < lowest_stepcount) {
						// reset actions tracking array
						actions_tracker = []
						// and lowest stepcount
						lowest_stepcount = cur_stepcount
					}
					// generate a list of all actions and the quantity of each
					// and store/add them to an actions tracking array
					let tmp_list = []
					stepcount_ary.forEach((s) => {
						if(s.steps > 0) {
							tmp_list.push(s.steps+'x '+s.name)
						}
					})
					if(i > 0) {
						tmp_list.push(i+'x '+action.name)
					}
					actions_tracker.push(optimizeActionsGrouping(tmp_list.concat(formatLastActions(dynamic_last_actions))))
				}
			}
			return lowest_stepcount
		})
		
		// let t = 0
		const calcSmithingActions = (() => {
			// test for the requested_offset (aka the "smithing value") being out of range or not set properly (not set at all is valid)
			let offset = parseInt(requested_offset.value)
			if((requested_offset.value !== '' && isNaN(offset)) || (!isNaN(offset) && (offset < 0 || offset > 150))) {
				listsOfSmithingActions.value = []
				return
			}
			// test for all smithing actions being unset
			if(lastActions.reduce((result, action) => { return result + (action.value !== 0 ? 1 : 0) }, 0) === 0) {
				listsOfSmithingActions.value = []
				return
			}
			// t = Date.now()
			listsOfSmithingActions.value = ['Calculating...']
			setTimeout((() => {
				let actions_with_lowest_stepcount = []
				let lowest_action_group_count = 99
				let getCount = ((last_action) => Array.isArray(last_action) ? last_action.length : 1)
				let calcWithLastActions = ((last_actions_list = lastActionsList.value, dynamic_last_actions = [], cur_lowest_stepcount = 99) => {
					for(var i = 0; i < getCount(last_actions_list[0]); i++) {
						let cur_action = Array.isArray(last_actions_list[0]) ? last_actions_list[0][i] : last_actions_list[0]
						if(last_actions_list.length > 1) {
							cur_lowest_stepcount = calcWithLastActions(last_actions_list.slice(1), dynamic_last_actions.concat(cur_action), cur_lowest_stepcount)
						} else {
							let stepcount = calcSmithingActionsRecursive(smithingActionsForCalc, dynamic_last_actions.concat(cur_action))
							let action_group_count = actions_tracker.length > 0 ? actions_tracker[0].length : 0
							// if this returned stepcount is lower than the current recorded one, record the last actions and associated smithing steps
							// otherwise discard this one
							if(stepcount < cur_lowest_stepcount || (stepcount === cur_lowest_stepcount && action_group_count < lowest_action_group_count)) {
								// record current actions and discard previously recorded ones
								actions_with_lowest_stepcount = deepClone(actions_tracker)
								cur_lowest_stepcount = stepcount
								lowest_action_group_count = action_group_count
								actions_tracker = []
							}
						}
					}
					return cur_lowest_stepcount
				})
				calcWithLastActions()
				listsOfSmithingActions.value = actions_with_lowest_stepcount
				// console.log('calcSmithingActions took '+(Date.now() - t)+'ms')
			}), 1000)
		})

		return {
			calcFrom,
			selected_calc_from,
			showUserDefinedSmithingValue,
			showHowToChooseCalcFrom,
			closeHowToChooseCalcFrom,
			show_how_to_choose_dialog,
			show_helper_dialog,
			showHelperDialog,
			closeHelperDialog,
			smithingHelperValues,
			smithingHelperTotal,
			useHelperValue,
			smithingActions,
			smithingActionsForSelect,
			lastActions,
			listsOfSmithingActions,
			requested_offset,
			calcSmithingActions,
			noSuggestions,
			isCalculating,
			hasSuggestions,
			show_more,
		}
	}
}).use(vuetify).mount('#app')
